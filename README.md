# Microservices Workshop
Lab 02: Meeting the Application

---

## Instructions


### Meet the application:

 - Browse to the application ui using the url:  http://35.195.214.244:3000/

 - Set 2 numbers and click "calculate"

 
### Meet the application API:

 - sum API: http://35.195.142.65:3001/sum/12/13


 - subtraction API:  http://35.195.142.65:3002/subtraction/8/2


 - multiplication API:  http://35.195.142.65:3003/multiplication/8/3


 - division API:  http://35.195.142.65:3004/division/15/3
 
 

### Inspect the sources:

 - sum-service:  https://github.com/selaworkshops/sum-service


 - subtraction-service:  https://github.com/selaworkshops/subtraction-service


 - multiplication-service:  https://github.com/selaworkshops/multiplication-service


 - division-service:  https://github.com/selaworkshops/division-service


 - ui-service:  https://github.com/selaworkshops/ui-service



 